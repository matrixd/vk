package com.example.vkopen;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.perm.kate.api.Message;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class Cache {
	private CacheOpenHelper helper;
	private Context context;
	
	public Cache(Context context) {
		this.helper = new CacheOpenHelper(context);
		this.context = context;
	}
	
	public static boolean write(Context context,String fname, byte buffer[]){
		Log.i("Cache", "writing " + fname);
		/* Checks if external storage is available for read and write */
		String state = Environment.getExternalStorageState();
		if (!Environment.MEDIA_MOUNTED.equals(state))
		    return false;
		File file = new File(context.getExternalFilesDir(null), fname);
		try {
			OutputStream os = new FileOutputStream(file);
			os.write(buffer);
			os.close();
		} catch(IOException e) {
			Log.w("ExternalStorage", "Error writing " + fname, e);
		}
		return true;
	}
	
	public static boolean exists(Context context, String fname){
		File file = new File(context.getExternalFilesDir(null), fname);
		/* Checks if external storage is available to read */
		String state = Environment.getExternalStorageState();
		/*if (!Environment.MEDIA_MOUNTED.equals(state) ||
			!Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
		    return false;*/
		Log.i("Cache", "ifexists "+fname+" "+file.exists());
		Log.i("Cache", "ifexists "+file.getAbsolutePath());
		if(!file.exists())
			return false;
		return true;
	}
	
	
	public static byte[] read(Context context, String fname){
		Log.i("Cache", "reading " + fname);
		byte[] res = new byte[0];
		File file = new File(context.getExternalFilesDir(null), fname);	
		try {
			FileInputStream is = new FileInputStream(file);
			res = new byte[((int)file.length())];
			int i = 0;
			while((res[i] = (byte) is.read()) != -1)
				i++;
			is.close();
		} catch(IOException e) {
			Log.w("ExternalStorage", "Error reading " + fname, e);
		}
		
		return res;
	}
	public static String getImgUri(Context context, String fname){
		File file = new File(context.getExternalFilesDir(null), fname);
		Log.i("Cache", "geturi: "+ file.getAbsolutePath());
		return file.getAbsolutePath();
	}
	public static String getImgFname(String url){
		String img = "";
    	//Pattern p = Pattern.compile("^http:\\/\\/(\\w+)\\.vk\\.\\w+\\/(\\w+)\\/(\\w+)\\/(\\S+)$g");
    	Pattern p = Pattern.compile("\\/(\\w+)");
    	Matcher m = p.matcher(url);
        while (m.find())
            img += m.group();
        img = img.replaceAll("/", "") + ".jpg";
        return img;
	}
	
	public void addUids(Map<Long, Object[]> map) {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values;
		for(Map.Entry<Long, Object[]> entry : map.entrySet()) {
			values = new ContentValues();
			values.put("uid", entry.getKey());
			values.put("first_name", (String) entry.getValue()[0]);
			values.put("last_name", (String) entry.getValue()[1]);
			db.insert(CacheOpenHelper.UIDS_TABLE_NAME, null, values);
		}
		db.close();
	}
	
	public String[] getUid(Long uid) {
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT first_name,last_name FROM "+CacheOpenHelper.UIDS_TABLE_NAME+" WHERE uid=?", new String[] {uid.toString()});
		String[] strs = new String[2];
		/*if (c.moveToFirst()) {
            do {
               strs[i] = 
            } while (cursor.moveToNext());
        }*/
		if(c.moveToFirst()) {
			strs[0] = c.getString(0);
			strs[1] = c.getString(1);
		}
		return strs;
	}
	
	private class CacheOpenHelper extends SQLiteOpenHelper {

	    private static final String DATABASE_NAME = "Cache";
		private static final int DATABASE_VERSION = 2;
	    
		public static final String UIDS_TABLE_NAME = "uids";
	    private static final String UIDS_TABLE_CREATE =
	                "CREATE TABLE " + UIDS_TABLE_NAME + " (" +
	                "uid" + " INTEGER, " +
	                "first_name" + " TEXT, " +
	                "last_name" + " TEXT," +
	                "PRIMARY KEY(uid DESC));";
	    
	    private static final String MESSAGES_TABLE_NAME = "messages";
	    private static final String MESSAGES_TABLE_CREATE =
	                "CREATE TABLE " + MESSAGES_TABLE_NAME + " (" +
	                "mid" + " INTEGER, " +
	                "uid" + " INTEGER, " +
	                "chat_id" + " INTEGER, " +
	                "obj" + " BLOB, " +
	                "PRIMARY KEY(mid DESC));";

	    CacheOpenHelper(Context context) {
	        super(context, DATABASE_NAME, null, DATABASE_VERSION);
	    }

	    @Override
	    public void onCreate(SQLiteDatabase db) {
	        db.execSQL(UIDS_TABLE_CREATE);
	        db.execSQL(MESSAGES_TABLE_CREATE);
	    }


		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			//if(oldVersion<newVersion){
			//	db.execSQL("DROP "+MESSAGES_TABLE_NAME+";");
			//	db.execSQL(MESSAGES_TABLE_CREATE);
			//}
		}
	}

	public void addMessage(Message msg) {
		//SerializableVk s = new SerializableVk();
		//s.msg = msg;
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values;
		values = new ContentValues();
		values.put("uid", msg.uid);
		values.put("mid", msg.mid);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream in;
		try {
			in = new ObjectOutputStream(baos);
			in.writeObject(msg);
			in.close();
			values.put("obj", baos.toByteArray());
			db.insert(CacheOpenHelper.MESSAGES_TABLE_NAME, null, values);
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();
	}
	
	public ArrayList<Message> getMessages(Long uid, Long chat_id, Long start_mid){
		SQLiteDatabase db = helper.getWritableDatabase();
		Cursor c;
		if(start_mid>0 && uid>0 && chat_id<1)
			c = db.rawQuery("SELECT obj FROM "+CacheOpenHelper.MESSAGES_TABLE_NAME+" WHERE uid = ? AND mid < ?" +
					//" ORDER BY mid ASC LIMIT 30;",
					" LIMIT 30;",
					new String[] {String.valueOf(uid),String.valueOf(start_mid)});
		else
			//c = db.rawQuery("SELECT obj FROM "+CacheOpenHelper.MESSAGES_TABLE_NAME+" WHERE uid = ? ORDER BY mid ASC LIMIT 30;",
			c = db.rawQuery("SELECT obj FROM "+CacheOpenHelper.MESSAGES_TABLE_NAME+" WHERE uid = ? LIMIT 30;",
					new String[] {String.valueOf(uid)});
		ArrayList<Message> lst = new ArrayList<Message>();
		while(c.moveToNext()) {
			Message msg = null;
			try {
				ObjectInputStream ois = new ObjectInputStream(
	                    new ByteArrayInputStream(c.getBlob(0)));
				msg = (Message) ois.readObject();
				lst.add(msg);
				Log.i("cache", msg.body);
			} catch (OptionalDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lst;
	}

	public Message getLast() {
		SQLiteDatabase db = helper.getWritableDatabase();
		Cursor c;
		c = db.rawQuery("SELECT obj FROM "+CacheOpenHelper.MESSAGES_TABLE_NAME+
				" LIMIT 1;",
				null);
		if(c.moveToFirst()){
			try {
				ObjectInputStream ois = new ObjectInputStream(
	                    new ByteArrayInputStream(c.getBlob(0)));
				return (Message) ois.readObject();
			} catch (OptionalDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
