package com.example.vkopen;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.json.JSONException;

import com.perm.kate.api.Api;
import com.perm.kate.api.KException;
import com.perm.kate.api.LastActivity;
import com.perm.kate.api.Message;

//import com.example.vkopen.ChatListActivity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

public class DialogActivity extends Activity {
	
	//ListView list;
	Api api;
	String nick;
	//String fullname;
	Long uid;
	Long chatId;
	Long cid;
	//Long lastMes;
	ArrayList<Message> dmessages;
	private ArrayList<Message> cmes;
	private ArrayList<Message> messages;
	private Main mBoundService;
	private boolean mIsBound = false;
	private LastActivity last;
	private MessageBroadCastReceiver mReceiver;
	private StatusBroadCastReceiver sReceiver;
	private ArrayAdapter<Message> adapter;
	private Cache cache;
	private Button header;
	private ProgressBar pheader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dialog);
		this.uid = (Long) getIntent().getExtras().get("uid");
		this.cid = (Long) getIntent().getExtras().get("cid");
		
		this.chatId = (long) 0;
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		cache = new Cache(this);
		
		pheader = new ProgressBar(this);
		
		header = new Button(this);
		header.setText("get more");
		header.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	((ListView)findViewById(R.id.dialogLst)).removeHeaderView(header);
            	((ListView)findViewById(R.id.dialogLst)).addHeaderView(pheader);
            	getMessages();
            }
		});
		
		this.messages = new ArrayList<Message>();
		
		((ListView)findViewById(R.id.dialogLst)).addHeaderView(pheader);
		
		this.adapter = new MessageArrayAdapter(this, R.layout.singlemsg,
				messages, nick);
        ((ListView)findViewById(R.id.dialogLst)).setAdapter(adapter);
		
		this.sReceiver = new StatusBroadCastReceiver();
		this.mReceiver = new MessageBroadCastReceiver();
		doBindService();
	}
	protected void getMessages(){
		Long lastMes;
		if(messages.size()>0)
			lastMes = messages.get(messages.size()-1).mid;
		else
			lastMes = (long) 0;
		
		cmes = cache.getMessages(uid, chatId, lastMes);
		if(cmes.size()>0) {
			//lastMes = cmes.get(cmes.size()-1).mid;
			messages.addAll(0,cmes);
		}
		
		Thread t = new Thread(){
            @Override
            public void run(){
            	try {
            		//simple horrible.
					dmessages = api.getMessagesHistory(uid, chatId, cid, (long) messages.size(), 30);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
		};
		if(cmes.size() < 30) {
			t.start();
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Log.i("DialogActivity", String.valueOf(messages.size()));
		printList();
	}
	
protected void getUserStatus(){
		
		Thread t = new Thread(){
            @Override
            public void run(){
            	try {
            		last = api.getLastActivity(uid);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
		};
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setStatus();
	}
	
	public void setStatus(){
		if(last.online) {
			mBoundService.names.setonline(uid);
			setTitle(((String)mBoundService.names.getOneString(uid)[0])+"   online");
		} else {
			mBoundService.names.setoffline(uid, last.last_seen);
			Calendar cal = Calendar.getInstance();
			Date l = new Date(cal.getTime().getTime()-last.last_seen);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
			setTitle(((String)mBoundService.names.getOneString(uid)[0])+"   last seen:"+df.format(last.last_seen)+" ago");
		}
	}
    
    protected void printList(){
    	Log.i("DialogActivity", "create adapter");
    	if(dmessages != null) {
	    	Collections.sort(dmessages, new MessageComparator());
	    	//for(int i=0; messages.size()>i; i++)
	    	//	Log.i("DialogActivity", String.valueOf(messages.get(i).date));
	    	for(int i=0; dmessages.size()>i; i++)
	    		cache.addMessage(dmessages.get(i));
	    	
	    	int i = 0;
	    	//for(int k = messages.size(); (k<30) && (i<cmes.size()); k++)
	    	//	messages.add(cmes.get(i));
	    	messages.addAll(0, dmessages);
	    	dmessages = null;
    	}
    	Log.i("DialogActivity", String.valueOf(messages.size()));
    	adapter.notifyDataSetChanged();
    	((ListView)findViewById(R.id.dialogLst)).removeHeaderView(pheader);
        ((ListView)findViewById(R.id.dialogLst)).addHeaderView(header);
    }
    public class MessageComparator implements Comparator<Message> {
        @Override
        public int compare(Message o1, Message o2) {
        	return Long.compare(o1.date, o2.date);
        }
    }
    private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
	        // This is called when the connection with the service has been
	        // established, giving us the service object we can use to
	        // interact with the service.  Because we have bound to a explicit
	        // service that we know is running in our own process, we can
	        // cast its IBinder to a concrete class and directly access it.
	        mBoundService = ((Main.LocalBinder)service).getService();

	        // Tell the user about this for our demo.
	        /*Toast.makeText(Binding.this, R.string.local_service_connected,
	                Toast.LENGTH_SHORT).show();*/
	        api = new Api(mBoundService.getToken(), Constants.API_ID);
	        setUserStatus(((Long)mBoundService.names.getOneString(uid)[1]));
	        nick = (String) mBoundService.names.get(uid)[0];
	        //getUserStatus();
	        getMessages();
	        mIsBound = true;
		    mBoundService.isActive(true);
	    }

	    public void onServiceDisconnected(ComponentName className) {
	        // This is called when the connection with the service has been
	        // unexpectedly disconnected -- that is, its process crashed.
	        // Because it is running in our same process, we should never
	        // see this happen.
	        mBoundService = null;
	       
	    }
	};

	void doBindService() {
	    // Establish a connection with the service.  We use an explicit
	    // class name because we want a specific service implementation that
	    // we know will be running in our own process (and thus won't be
	    // supporting component replacement by other applications).
	    bindService(new Intent(DialogActivity.this, 
	            Main.class), mConnection, Context.BIND_AUTO_CREATE);
	}

	void doUnbindService() {
	    if (mIsBound) {
	        // Detach our existing connection.
	        unbindService(mConnection);
	        mIsBound = false;
	    }
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    doUnbindService();
	}
	@Override
	protected void onStart(){
		super.onStart();
		//doBindService();
		IntentFilter intentFilter = new IntentFilter("vk.poll.userStatus");
		this.registerReceiver(sReceiver, intentFilter);
		intentFilter = new IntentFilter("vk.poll.message");
		this.registerReceiver(mReceiver, intentFilter);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		if(mIsBound)
			this.mBoundService.isActive(false);
		this.unregisterReceiver(this.mReceiver);
		this.unregisterReceiver(this.sReceiver);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		if(mIsBound)
			this.mBoundService.isActive(true);
	}
	
	private class MessageBroadCastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			//extract our message from intent
			Message msg = ((SerializableVk) intent.getSerializableExtra("message")).msg;
			if(msg.uid == uid) {
				messages.add(msg);
				adapter.notifyDataSetChanged();
				Log.i("Dialog", "recived");
				Log.i("Dialog", msg.body);
			}
			//log our message value
		}
		
	}
	
	private class StatusBroadCastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("ChatListActivity", "recivedstatus");
			//extract our message from intent
			Long u = intent.getLongExtra("uid", -1);
			if(u == uid) {
				setUserStatus(Long.valueOf(intent.getLongExtra("online", -1)));
			}
			//log our message value
		}
		
	}
	
	private void setUserStatus(Long time) {
		if(time == 1) {
			setTitle(((String)mBoundService.names.getOneString(uid)[0])+"   online");
			return;
		}
        if(time == 0) {
			setTitle(((String)mBoundService.names.getOneString(uid)[0])+"   offline");
			return;
        }
        if(time < 0) {
        	getUserStatus();
        	return;
        }
        if(time > 1) {
			setTitle(((String)mBoundService.names.getOneString(uid)[0])+"   last seen:");
	        Calendar cal = Calendar.getInstance();;
			Date l = new Date(cal.getTime().getTime()-last.last_seen);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
			setTitle(((String)mBoundService.names.getOneString(uid)[0])+"   last seen:"+df.format(l)+" ago");
			return;
        }
	}
	
}
