package com.example.vkopen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import com.perm.kate.api.Api;
import com.perm.kate.api.KException;
import com.perm.kate.api.User;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class Names {
	//uid,{string first_name, string last_name, online(0 - unknown, 1 - online, >1 - last online)}
	private Map<Long, Object[]> map = new HashMap<Long, Object[]>();
	Map<Long, Object[]> cmap;
	private final Api api;
	private ArrayList<Long> ids = new ArrayList<Long>();
	public boolean needToDownload=false;
	private Context context;
	Cache cache;
	
	public Names(Context context, Api vkapi){
		this.api = vkapi;
		this.context = context;
		this.cache = new Cache(context);
	}
	
	public Object[] get(Long uid){
		if(map.containsKey(uid)){
			return map.get(uid);
		} else {
			if(cache.getUid(uid)[0] == null){
				ids.add(uid);
				needToDownload = true;
				return new Object[] {String.valueOf(uid), null, Long.valueOf(0)};
			} else {
				map.put(uid, new Object[] {cache.getUid(uid)[0], cache.getUid(uid)[1], Long.valueOf(0)});
				return get(uid);
			}
		}
	}
	
	public Object[] getOneString(Long uid){
		return new Object[] {(String) get(uid)[0]+" "+(String) get(uid)[1], get(uid)[2]};
	}
	
	
	private class GetNicks extends AsyncTask<Void, Void, Void> {

	    private Exception exception;

	    protected Void doInBackground(Void... arg0) {
	    	Log.i("Names", "startingDownload01");
			ArrayList<User> users;
			cmap = new HashMap<Long, Object[]>();
			try {
				Log.i("Names", "startingDownload");
				users = api.getProfiles(ids, null, null, null, null, null);
				ids.clear();
				Log.i("Names", "startingDownload11");
				for(int i = 0; i<users.size(); i++)
					cmap.put(users.get(i).uid, new Object[] {users.get(i).first_name, users.get(i).last_name, Long.valueOf(0)});
				Log.i("Names", "startingDownload12");
				cache.addUids(cmap);
				Log.i("Names", "startingDownload13");
				map.putAll(cmap);
				Log.i("Names", "startingDownload14");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	    }
	}
	
	public void download(){
		new GetNicks().execute();
		this.needToDownload = false;
	}
	Runnable successRunnable=new Runnable(){
        @Override
        public void run() {
        	cache.addUids(cmap);
			map.putAll(cmap);
        }
		
    };
	
	public void setonline(Long uid) {
		if(map.containsKey(uid))
			map.get(uid)[2] = Long.valueOf(1);
	}
	public void setoffline(Long uid, Long time) {
		if(map.containsKey(uid))
			map.get(uid)[2] = time;
	}
}
