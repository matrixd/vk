package com.example.vkopen;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.perm.kate.api.Message;

public class DialogsArrayAdapter extends ArrayAdapter<Message> {

	    private final ArrayList<Message> list;
	    private final Activity context;

	    public DialogsArrayAdapter(Activity context, ArrayList<Message> list) {
	        super(context, R.layout.msglistitem, list);
	        this.context = context;
	        this.list = list;
	    }

	    static class ViewHolder {
	        protected TextView usern;
	        protected TextView text;
	        protected TextView status;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View view = null;
	        if (convertView == null) {
	            LayoutInflater inflator = context.getLayoutInflater();
	            view = inflator.inflate(R.layout.msglistitem, null);
	            final ViewHolder holder = new ViewHolder();
	            holder.usern = (TextView) view.findViewById(R.id.name);
	            holder.text = (TextView) view.findViewById(R.id.body);
	            holder.status = (TextView) view.findViewById(R.id.status);
	            
	            String members = "";
	            ChatListActivity p = (ChatListActivity) context;
	            Object[] tmp  = p.mBoundService.names.getOneString(list.get(position).uid);
	            members = (String) tmp[0];
	            if((Long)tmp[1] < 1)
	            	holder.status.setText("offline");
	            if((Long)tmp[1] == 1)
	            	holder.status.setText("online");
	            if((Long)tmp[1] > 1) {
		            Calendar cal = Calendar.getInstance();
					Date l = new Date(cal.getTime().getTime()-(Long)tmp[2]);
					SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
					holder.status.setText(((String)tmp[0])+"   last seen:"+df.format(l)+" ago");
	            }
				
	            holder.usern.setText(members);
	            holder.text.setText(list.get(position).body);
	        }
	        return view;
	    }
}
