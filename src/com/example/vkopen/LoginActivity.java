package com.example.vkopen;

import com.perm.kate.api.Auth;

import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.graphics.Bitmap;
import com.example.vkopen.Constants;
import com.example.vkopen.PollListener;

public class LoginActivity extends Activity {
	WebView webview;
	
	private Main mBoundService;
	private boolean mIsBound = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		//PollListener poll = (PollListener) getParent();
		webview = (WebView) findViewById(R.id.wv1);
		
		webview.getSettings().setJavaScriptEnabled(true);
		webview.clearCache(true);
		webview.setWebViewClient(new VkWebViewClient());
		
		CookieSyncManager.createInstance(this);
		
		//perrmissions
		String url = Auth.getUrl(Constants.API_ID, "friends,messages");		
		webview.loadUrl(url);
		doBindService();
	}
	
	 class VkWebViewClient extends WebViewClient {
	        @Override
	        public void onPageStarted(WebView view, String url, Bitmap favicon) {
	            super.onPageStarted(view, url, favicon);
	            parseUrl(url);
	        }
	 }
	 private void parseUrl(String url) {
	        try {
	            if(url==null)
	                return;
	            if(url.startsWith(Auth.redirect_url)) {
	                if(!url.contains("error=")){
	                    String[] auth=Auth.parseRedirectUrl(url);
	                    //Intent intent=new Intent();
	                    //intent.putExtra("token", auth[0]);
	                    //intent.putExtra("user_id", Long.parseLong(auth[1]));
	                    //setResult(Activity.RESULT_OK, intent);
	                    mBoundService.logon(auth[0], auth[1]);
	                }
	                finish();
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	 }
	 private ServiceConnection mConnection = new ServiceConnection() {
			@Override
			public void onServiceConnected(ComponentName className, IBinder service) {
		        // This is called when the connection with the service has been
		        // established, giving us the service object we can use to
		        // interact with the service.  Because we have bound to a explicit
		        // service that we know is running in our own process, we can
		        // cast its IBinder to a concrete class and directly access it.
		        mBoundService = ((Main.LocalBinder)service).getService();

		        // Tell the user about this for our demo.
		        /*Toast.makeText(Binding.this, R.string.local_service_connected,
		                Toast.LENGTH_SHORT).show();*/
		    }

		    public void onServiceDisconnected(ComponentName className) {
		        // This is called when the connection with the service has been
		        // unexpectedly disconnected -- that is, its process crashed.
		        // Because it is running in our same process, we should never
		        // see this happen.
		        mBoundService = null;
		       
		    }
		};

		void doBindService() {
		    // Establish a connection with the service.  We use an explicit
		    // class name because we want a specific service implementation that
		    // we know will be running in our own process (and thus won't be
		    // supporting component replacement by other applications).
		    bindService(new Intent(LoginActivity.this, 
		            Main.class), mConnection, Context.BIND_AUTO_CREATE);
		    this.mIsBound = true;
		}

		void doUnbindService() {
		    if (mIsBound) {
		        // Detach our existing connection.
		        unbindService(mConnection);
		        mIsBound = false;
		    }
		}

		@Override
		protected void onDestroy() {
		    super.onDestroy();
		    doUnbindService();
		}
		
		@Override
		protected void onStart(){
			super.onStart();
			doBindService();
		}

}
