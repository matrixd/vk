package com.example.vkopen;

import com.example.vkopen.Cache;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perm.kate.api.Message;
import com.perm.kate.api.Photo;

public class MessageArrayAdapter extends ArrayAdapter<Message> {
	
	private Context context;
	private ArrayList<Message> msgs;
	private SimpleDateFormat df;
	private String nick;
	
	static class ViewHolder {
        protected TextView body;
        protected LinearLayout lay;
    }

	public MessageArrayAdapter(Context context, int resource,  ArrayList<Message> objects, String nick) {
		super(context, resource, objects);
		//this.res = resource;
		this.context = context;
		this.msgs = objects;
		this.nick = nick;
		this.df = new SimpleDateFormat("(HH:mm:ss) ");
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder = null;
		if(convertView == null) {
		    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    view = inflater.inflate(R.layout.singlemsg, null);
		    holder = new ViewHolder();
		    holder.body = (TextView) view.findViewById(R.id.msgBody);
		    holder.lay = (LinearLayout) view.findViewById(R.id.LinearLayout1);
		    
		    for(int i=0; msgs.get(position).attachments.size()>i; i++) {
		    	ImageView child;
		    	if(msgs.get(position).attachments.get(i).photo != null){
			    	child = new ImageView(context);
			    	String img = Cache.getImgFname(msgs.get(position).attachments.get(i).photo.src);
			    	if(Cache.exists(context, img)) {
			    		//Bitmap myBitmap = BitmapFactory.decodeFile(Cache.getImgUri(context, img));
			    		child.setImageURI(Uri.parse(Cache.getImgUri(context, img)));
			    		//child.setImageBitmap(myBitmap);
			    	} else
			    		new DownloadImageTask(child).execute(msgs.get(position).attachments.get(i).photo.src);
			    	child.setPadding(10, 10, 10, 10);
			    	child.setClickable(true);
			    	child.setOnClickListener(new OpenFullImg(msgs.get(position).attachments.get(i).photo));
			    	holder.lay.addView(child);
			    	//parent.addView(child);
		    	}
		    	if(msgs.get(position).attachments.get(i).document != null) {
		    		TextView txt = new TextView(context);
		    		String str;
		    		str = "<a href=\""+msgs.get(position).attachments.get(i).document.url+"\">";
		    		str += msgs.get(position).attachments.get(i).document.title+"</a>";
		    		txt.setText(Html.fromHtml(str));
		    		
		    		holder.lay.addView(txt);
		    	}
		    	if(msgs.get(position).attachments.get(i).audio != null) {
		    		TextView txt = new TextView(context);
		    		String str;
		    		str = "<a href=\""+msgs.get(position).attachments.get(i).audio.url+"\">";
		    		str += msgs.get(position).attachments.get(i).audio.title+"</a>";
		    		txt.setText(Html.fromHtml(str));
		    		
		    		holder.lay.addView(txt);
		    	}
		    	if(msgs.get(position).attachments.get(i).graffiti != null) {
		    		child = new ImageView(context);
			    	String img = Cache.getImgFname(msgs.get(position).attachments.get(i).photo.src);
			    	if(Cache.exists(context, img)) {
			    		//Bitmap myBitmap = BitmapFactory.decodeFile(Cache.getImgUri(context, img));
			    		child.setImageURI(Uri.parse(Cache.getImgUri(context, img)));
			    		//child.setImageBitmap(myBitmap);
			    	} else
			    		new DownloadImageTask(child).execute(msgs.get(position).attachments.get(i).graffiti.src);
			    	child.setPadding(10, 10, 10, 10);
			    	child.setClickable(true);
			    	//child.setOnClickListener(new OpenFullImg(msgs.get(position).attachments.get(i).photo));
			    	holder.lay.addView(child);
			    	//parent.addView(child);
		    	}
		    }
		    view.setTag(holder);
		} else
			holder = (ViewHolder)convertView.getTag();
	    String text = "";
	    text += df.format(new Date(msgs.get(position).date));
	    if(msgs.get(position).is_out)
	    	text += "<b>me:</b> ";
	    else
	    	text += "<b>" + nick + ":</b> ";
	    text += msgs.get(position).body;
	    holder.body.setText(Html.fromHtml(text));
	    Linkify.addLinks(holder.body,Linkify.ALL);
	    if(!msgs.get(position).read_state)
	    	holder.lay.setBackgroundColor(000000000);
	    return view;
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		  ImageView bmImage;

		  public DownloadImageTask(ImageView bmImage) {
		      this.bmImage = bmImage;
		  }

		  protected Bitmap doInBackground(String... urls) {
		      String url = urls[0];
		      Bitmap preview = null;
		      Log.i("MessageAdapter", "Download "+url);
		      try {
		        InputStream in = new java.net.URL(url).openStream();
		        byte[] buf = new byte[in.available()];
		        /*while((buf[i] = (byte) in.read())!=-1)
		        	i++;*/
		        in.read(buf);
		        Cache.write(context, Cache.getImgFname(url), buf);
		        //in.reset();
		        //preview = BitmapFactory.decodeStream(in);
		        in.close();
		        preview = BitmapFactory.decodeByteArray(buf, 0, buf.length);
		      } catch (Exception e) {
		          Log.e("Error", e.getMessage());
		          e.printStackTrace();
		      }
		      return preview;
		  }

		  protected void onPostExecute(Bitmap result) {
		      bmImage.setImageBitmap(result);
		  }
	}
	
	private class OpenFullImg implements View.OnClickListener {
		
		private Photo p;
		
		OpenFullImg(Photo photo){
			p = photo;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
		}
		
	}

}
