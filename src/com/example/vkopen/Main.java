package com.example.vkopen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;

import com.perm.kate.api.Api;
import com.perm.kate.api.KException;
import com.perm.kate.api.Message;
import com.perm.kate.api.User;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class Main extends Service {
	
	private boolean isPollLaunched = true;
	public Api api;
	public Names names;
	private UpdateTokenReceiver tReceiver;
	private MsgReceiver mReceiver;
	private int states = 0;
	private Intent pollservice;
	
	// This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();
    
    public class LocalBinder extends Binder {
    	Main getService() {
            return Main.this;
        }
    }
	
	public Account account;

	@Override
	public void onCreate(){
		Log.i("main", "starting a service");
		account = new Account();
		names = new Names(this,api);//!!!
		IntentFilter intentFilter = new IntentFilter("vk.updateToken");
		this.tReceiver = new UpdateTokenReceiver();
		this.registerReceiver(tReceiver, intentFilter);
		
		if(account.access_token == null){
			startLoginActivity();
			isPollLaunched = false;
		} else {
			isPollLaunched = true;
			startPollListener();
		}
		this.mReceiver = new MsgReceiver();
	}

	@Override
	public void onDestroy() {
		Log.i("main", "stop");
		this.unregisterReceiver(tReceiver);
		stopService(pollservice);
	}
	
	@Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    
	private void startPollListener(){
		Log.i("main", "starting a poll service");
		pollservice =new Intent(this,PollListener.class);
		pollservice.putExtra("token", account.access_token);
		pollservice.putExtra("id", account.user_id);
		//i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startService(pollservice);
	}
	
	public void startLoginActivity() {
		Log.i("main", "starting login");
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplication().startActivity(intent);
    }
	
	public void logon(String token, String user_id) {
		this.account.access_token=token;
		this.account.user_id=Long.valueOf(user_id);
		account.save(getBaseContext());
		this.api = new Api(token, Constants.API_ID);
		this.names = new Names(this, this.api);
		
		Intent intent = new Intent("vk.updatedToken");
		intent.putExtra("token", token);
		this.sendBroadcast(intent);
		
		if(!isPollLaunched){
			isPollLaunched = true;
			startPollListener();
		}
	}
	
	private void notify(String text){
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(this)
		        .setSmallIcon(R.drawable.ic_launcher)
		        .setContentTitle("Vk")
		        .setContentText(text);
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(this, ChatListActivity.class);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(ChatListActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		int mId = 500;
		mNotificationManager.notify(mId, mBuilder.build());
	}
	
	
	//-----------------------Reciving Broadcasts---------------------
	
	private class UpdateTokenReceiver extends BroadcastReceiver{
		
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("Main", "updtoken");
			startLoginActivity();
		}	
	}
	
	private class MsgReceiver extends BroadcastReceiver{
		
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("Main", "newmsg");
			Message msg = ((SerializableVk) intent.getSerializableExtra("message")).msg;
			Main.this.notify("You've got new message from " + names.getOneString(msg.uid)[0]);
		}
	}


	public String getToken() {
		// TODO Auto-generated method stub
		return account.access_token;
	}

	public long getUserId() {
		// TODO Auto-generated method stub
		return account.user_id;
	}
	
	//
	public void isActive(boolean active) {		
		
		if(active) {
			//if(states == 0)
				//this.unregisterReceiver(this.mReceiver);
			states++;
		}
		else {
			if(states == 1)	{
				IntentFilter intentFilter = new IntentFilter("vk.poll.message");
				this.registerReceiver(mReceiver, intentFilter);
			}
			states--;
		}
		
		
	}
}
