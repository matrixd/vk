package com.example.vkopen;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.perm.kate.api.Api;
import com.perm.kate.api.Attachment;
import com.perm.kate.api.KException;
import com.perm.kate.api.Message;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class PollListener extends IntentService {
	
		
	private Api api;
	private String resMessage;
	private String srv;
	private Long ts;
	private String key;
	private Cache cache;
	private Long accountId;
	private UpdateTokenReceiver tReceiver;
	/**
	* A constructor is required, and must call the super IntentService(String)
	* constructor with a name for the worker thread.
	*/
	public PollListener() {
		super("PollListener");
	}

	/**
	* The IntentService calls this method from the default worker thread with
	* the intent that started the service. When this method returns, IntentService
	* stops the service, as appropriate.
	*/
	@Override
	protected void onHandleIntent(Intent intent) {
	      // Normally we would do some work here, like download a file.
	      // For our sample, we just sleep for 5 seconds.
		Log.i("PollListener", "active");
		cache = new Cache(this);
		String token = intent.getStringExtra("token");
		accountId = intent.getLongExtra("id", 0);
		if(token != null){
			newToken(token);
			getMessages();
			longPoll();
		}
	}
	@Override
	public void onCreate() {
		super.onCreate();
		Log.i("PollListener", "create");
		/*IntentFilter intentFilter = new IntentFilter("vk.updatedToken");
		this.tReceiver = new UpdateTokenReceiver();
		this.registerReceiver(tReceiver, intentFilter);*/
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i("PollListener", "exit");
		this.unregisterReceiver(tReceiver);
	}
	
	private void newToken(String t){
		api = new Api(t, Constants.API_ID);
	}
	
	private void getMessages(){
		ArrayList<Message> msgs = new ArrayList<Message>();
		try {
			Message msg = cache.getLast();
			if(msg != null) {
				Long offset = msg.date;
				msgs.addAll(api.getMessages(offset, false, 200));
				msgs.addAll(api.getMessages(offset, true, 200));
				for(Message m : msgs) {
					cache.addMessage(m);
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void longPoll(){
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InputStream stream = null;
		try {
			String u = urlCreate();
			if(u==null)
				return;
			URL url = new URL(u);
			stream = url.openConnection().getInputStream();
		    InputStreamReader reader = new InputStreamReader(stream);
		    int next = -1;
		    resMessage = "";
		    while ((next = reader.read()) != -1) {
		    	resMessage += (char) next;
		    }
		    Log.i("PollListener", resMessage);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.i("PollListener", "success");
		if (resMessage != null) {
			try {
				JSONObject json = new JSONObject(resMessage);
				this.ts = Long.valueOf(((String) json.getString("ts")));
				JSONArray jArray = json.getJSONArray("updates");
				for(int i=0; jArray.length()>i; i++)
					parseUpdate(jArray.getJSONArray(i));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			resMessage = null;
		}
		longPoll();
	}
	
	private String urlCreate(){
		if(srv == null) {
			try {
				Object[] res = api.getLongPollServer(null,null);
				this.ts = (Long) res[2];
				this.srv = (String) res[1];
				this.key = (String) res[0];
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (KException e) {
				if(e.error_code == 5)
    				updateToken();
				e.printStackTrace();
				return null;
			}
		}
		//"https://{$server}?act=a_check&key={$key}&ts={$ts}&wait=25&mode=2"
		if(srv!=null) {
			Log.i("PollListener", String.format("http://%s?act=a_check&key=%s&ts=%d&wait=25&mode=2", srv, key, ts));
			return String.format("http://%s?act=a_check&key=%s&ts=%d&wait=25&mode=2", srv, key, ts);
		}
		return null;
	}
	  
	  /*@Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
	      Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
	      return super.onStartCommand(intent,flags,startId);
	  }*/
	

	//-------------------------Broadcasts---------------------

	private void parseUpdate(JSONArray obj){
		try {
			int upd = obj.getInt(0);
			Intent intent;
			switch(upd){
			//4,$message_id,$flags,$from_id,$timestamp,$subject,$text,$attachments -- добавление нового сообщения
			case 4:
				Log.i("PollListener", "begin to parse message");
				Message msg = null;
				if(obj.getJSONArray(7) != null){
					ArrayList<Long> lst = new ArrayList<Long>();
					lst.add(obj.getLong(1));
					try {
						msg = api.getMessagesById(lst).get(0);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} catch (KException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					}
				} else {
					msg = new Message();
					msg.attachments = new ArrayList<Attachment>();
					msg.mid = obj.getLong(1);
					msg.uid = obj.getLong(3);
					if((obj.getInt(1) % 2)>1)
						msg.read_state = false;
					else
						msg.read_state = true;
					msg.date = obj.getLong(4);
					msg.title = obj.getString(5);
					msg.body = obj.getString(6);
					if(msg.uid == accountId)
						msg.is_out = false;
					else
						msg.is_out = true;
				}
				Log.i("PollListener", msg.body);
				SerializableVk s = new SerializableVk();
				s.msg = msg;
				cache.addMessage(msg);
				intent = new Intent("vk.poll.message").putExtra("message", s);
				this.sendBroadcast(intent);
				break;	
			//offline
			case 8:
				intent = new Intent("vk.poll.userStatus");
				intent.putExtra("uid", obj.getLong(1)*-1);
				intent.putExtra("online", Long.valueOf(1));
				this.sendBroadcast(intent);
				break;
			//online
			case 9:
				intent = new Intent("vk.poll.userStatus");
				intent.putExtra("uid", obj.getLong(1)*-1);
				intent.putExtra("online", Long.valueOf(0));
				this.sendBroadcast(intent);
				break;
			//users types
			//61,$user_id,$flags
			/*case 61:
				Long uid = obj.getLong(1);
				break;*/
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void updateToken(){
		Intent intent = new Intent("vk.updateToken");
		//intent.putExtra("uid", obj.getLong(1)*-1);
		//intent.putExtra("online", Long.valueOf(0));
		sendBroadcast(intent);
	}
	
	//-----------------------Reciving Broadcasts---------------------
	
	private class UpdateTokenReceiver extends BroadcastReceiver{
	
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("PollListener", "token recived");
			newToken(intent.getStringExtra("token"));
			getMessages();
			longPoll();
		}
	
	}
}
