package com.example.vkopen;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;

import com.example.vkopen.*;
import android.os.Bundle;
import android.os.IBinder;

import com.perm.kate.api.Api;
import com.perm.kate.api.KException;
import com.perm.kate.api.Message;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

public class ChatListActivity extends ListActivity {

	Api api;
	ArrayList<Message> msgs = new ArrayList<Message>();
	public Main mBoundService;
	private boolean mIsBound = false;
	private MessageBroadCastReceiver mReceiver;
	private StatusBroadCastReceiver sReceiver;
	private TokenBroadCastReceiver tReceiver;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Create a progress bar to display while the list loads
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setIndeterminate(true);
        getListView().setEmptyView(progressBar);

        // Must add the progress bar to the root of the layout
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        root.addView(progressBar);
        doBindService();
		
		//setContentView(R.layout.activity_chat_list);
        
        this.mReceiver = new MessageBroadCastReceiver();
		this.sReceiver = new StatusBroadCastReceiver();
		this.tReceiver = new TokenBroadCastReceiver();
		
	}
	
	protected void showList(){
		Log.i("ChatList", "showlist");
		new Thread(){
            @Override
            public void run(){
                try {
                	msgs = api.getMessagesDialogs(0,10,null,null);
        			if(msgs.size() > 0){
        				Log.i("ChatList", "got messages");
        				runOnUiThread(successRunnable);
        			}
            	} catch (IOException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		} catch (JSONException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		} catch (KException e) {
	    			//expired token
	    			//if(e.error_code == 5)
	    			//	mBoundService.startLoginActivity();
	    			e.printStackTrace();
	    		}
            }
        }.start();
	}
	
	protected void printList(ArrayList<Message> messages){
		Log.i("ChatList", "printlist");
		if(messages.size() > 0){
			ArrayAdapter<Message> adapter = new DialogsArrayAdapter(this,
					messages);
	        setListAdapter(adapter);
		}
		updateNicks();
	}
	
	protected void updateNicks(){
		if(mBoundService.names.needToDownload)
			mBoundService.names.download();
		/*Log.i("ChatList", "download nicks");
		if(!mBoundService.names.needToDownload)
			return;
		
		new Thread(){
            @Override
            public void run(){
            	mBoundService.names.download();
            	runOnUiThread(successRunnable);
            }
		}.start();*/
	}
	
	Runnable successRunnable=new Runnable(){
        @Override
        public void run() {
        	printList(msgs);
        }
		
    };
	
	@Override 
    public void onListItemClick(ListView l, View v, int position, long id) {
		//updateNicks();
		//mBoundService.names.download();
		Long uid = ((Message) l.getAdapter().getItem(position)).uid;
		Long chatId = ((Message) l.getAdapter().getItem(position)).mid;
		if(uid != null)
			Log.i("ChatList", uid.toString());
		if(chatId != null)
			Log.i("ChatList", chatId.toString());
		//updateNicks();
		Intent intent = new Intent(this, DialogActivity.class);
		intent.putExtra("uid", uid);
		intent.putExtra("chatId", chatId);
		intent.putExtra("nick", (String) mBoundService.names.get(uid)[0]);
		if(mBoundService.names.get(uid).length > 2)
			intent.putExtra("online", (Long) mBoundService.names.get(uid)[2]);
		else
			intent.putExtra("online", Long.valueOf(-1));
		intent.putExtra("token", mBoundService.getToken());
		intent.putExtra("apiId", Constants.API_ID);
		intent.putExtra("cid", mBoundService.getUserId());
		startActivity(intent);
    }
	
	private void gotToken(String token) {
		Log.i("ChatList", "got a token");
    	this.api = new Api(token, Constants.API_ID);
    	showList();
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
	        // This is called when the connection with the service has been
	        // established, giving us the service object we can use to
	        // interact with the service.  Because we have bound to a explicit
	        // service that we know is running in our own process, we can
	        // cast its IBinder to a concrete class and directly access it.
	        mBoundService = ((Main.LocalBinder)service).getService();

	        // Tell the user about this for our demo.
	        /*Toast.makeText(Binding.this, R.string.local_service_connected,
	                Toast.LENGTH_SHORT).show();*/
	        Log.i("ChatList", "connected to service");
	        if(mBoundService.getToken() != null){
	        	gotToken(mBoundService.getToken());
	        }
	        mBoundService.isActive(true);
	        mIsBound = true;
	    }

	    public void onServiceDisconnected(ComponentName className) {
	        // This is called when the connection with the service has been
	        // unexpectedly disconnected -- that is, its process crashed.
	        // Because it is running in our same process, we should never
	        // see this happen.
	        mBoundService = null;
	       
	    }
	};

	private void doBindService() {
	    // Establish a connection with the service.  We use an explicit
	    // class name because we want a specific service implementation that
	    // we know will be running in our own process (and thus won't be
	    // supporting component replacement by other applications).
	    bindService(new Intent(ChatListActivity.this, 
	            Main.class), mConnection, Context.BIND_AUTO_CREATE);
	}

	void doUnbindService() {
	    if (mIsBound) {
	        // Detach our existing connection.
	        unbindService(mConnection);
	        mIsBound = false;
	    }
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    doUnbindService();
	    //this.unregisterReceiver(this.mReceiver);
	    //this.unregisterReceiver(this.sReceiver);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		if(mIsBound)
			this.mBoundService.isActive(false);
		this.unregisterReceiver(this.mReceiver);
		this.unregisterReceiver(this.sReceiver);
		this.unregisterReceiver(this.tReceiver);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		if(mIsBound) {
			this.mBoundService.isActive(true);
			if(mBoundService.getToken() != null)
				gotToken(mBoundService.getToken());
		}
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		IntentFilter intentFilter = new IntentFilter("vk.poll.userStatus");
		this.registerReceiver(sReceiver, intentFilter);
		intentFilter = new IntentFilter("vk.poll.message");
		this.registerReceiver(mReceiver, intentFilter);
		intentFilter = new IntentFilter("vk.updatedToken");
		this.registerReceiver(tReceiver, intentFilter);
	}
	
	private class MessageBroadCastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("ChatListActivity", "recived");
			//extract our message from intent
			Message msg = ((SerializableVk) intent.getSerializableExtra("message")).msg;
			//log our message value
			Log.i("ChatListActivity", msg.body);
		}
		
	}
	
	//vk.updatedToken
	private class TokenBroadCastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("ChatListActivity", "recived token");
			//extract our message from intent
			gotToken(intent.getStringExtra("token"));
		}
		
	}
	
	private class StatusBroadCastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("ChatListActivity", "recivedstatus");
			//extract our message from intent
			Long uid = intent.getLongExtra("uid", -1);
			if(intent.getLongExtra("online", -1) > 0)
				Log.i("ChatListActivity", String.valueOf(uid)+" comes online");
			else
				Log.i("ChatListActivity", String.valueOf(uid)+" comes offline");
			//log our message value
		}
		
	}

}
